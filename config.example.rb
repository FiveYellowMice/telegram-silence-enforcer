# Username of the bot, without preceding '@'
@telegram_username = ENV['TELEGRAM_SILENCE_ENFORCER_TELEGRAM_USERNAME']

# Telegram API token, acquired from BotFather
@telegram_api_token = ENV['TELEGRAM_SILENCE_ENFORCER_TELEGRAM_API_TOKEN']

# Path to store SQLite database
@database_path = ENV['TELEGRAM_SILENCE_ENFORCER_DATABASE_PATH']
