# Telegram Silence Enforcer

**As Telegram gets native user restrictions now, Telegram Silence Enforcer has been deprecated. This repository is reserved for only historical references.**


Bring 'moderated', 'voice' and 'quiet' to Telegram.

## Concepts

The concepts are mostly inspired by similar features in IRC:

* A group has a 'moderated' status, if a group is moderated, all users except users with 'voice' status can not talk.
* A user has a 'voice' status and a 'quiet' status. Voice users can talk while the group is moderated. Quiet users can not talk at any time.
* The status of groups and users can be changed by group administartors.
* The 'voice' status is given to administartors automatically.

## Installation

Make sure you have fulfilled these requirements:

* Ruby MRI 2.4.1
* Bundler
* A Telegram bot token acquired from BotFather

You can use RVM or rbenv if the version of Ruby your distro provides is too old.

Then, clone the repository.

Copy `config.example.rb` to `config.rb`, make your changes.

Run `bin/silebot`.

## Usage

* `/mode +m [expire=1y2M3d4h5m6s]`  
  Enable moderated mode for this group. In moderated mode, any user without voice flag can not talk.

* `/mode -m`  
  Disable moderated mode for this group.

* `/mode +v @username [expire=1y2M3d4h5m6s]`  
  Add voice flag to a user. When the group is moderated, such user can talk.

* `/mode -v @username`  
  Remove voice flag from a user.

* `/mode +q @username [expire=1y2M3d4h5m6s]`  
  Add quiet flag to a user. A user can not talk with this flag.

* `/mode -q @username`  
  Remove quiet flag from a user.

You can reply to a user's message instead of using that user's username.

## Copyright

Copyright (C) 2017  FiveYellowMice

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
