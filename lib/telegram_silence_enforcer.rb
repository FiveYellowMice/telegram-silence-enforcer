# frozen_string_literal: true

module TelegramSilenceEnforcer

  autoload :Bot, 'telegram_silence_enforcer/bot'
  autoload :Utils, 'telegram_silence_enforcer/utils'
  autoload :Config, 'telegram_silence_enforcer/config'
  autoload :Command, 'telegram_silence_enforcer/command'
  autoload :BotOperationError, 'telegram_silence_enforcer/bot_operation_error'
  autoload :DB, 'telegram_silence_enforcer/db'
  autoload :VERSION, 'telegram_silence_enforcer/version'

  def self.start
    config = Config.new(ARGV)
    Bot.new(config)
  end

end
