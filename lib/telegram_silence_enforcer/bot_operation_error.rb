# frozen_string_literal: true

module TelegramSilenceEnforcer

  class BotOperationError < StandardError
  end

end
