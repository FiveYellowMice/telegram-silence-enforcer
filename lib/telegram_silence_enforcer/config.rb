# frozen_string_literal: true

require 'optparse'

module TelegramSilenceEnforcer

  class Config

    attr_reader :debug, :telegram_username, :telegram_api_token, :database_path

    def initialize(argv)

      config_file_name = 'config.rb'

      @debug = false

      OptionParser.new do |opts|
        opts.banner = "Usage: silebot [options]"

        opts.on('-c', '--config FILE', 'Specify configu file (default: config.rb)') do |arg|
          config_file_name = arg
        end

        opts.on('-D', '--debug', 'Turn on debug output') do
          @debug = true
        end

        opts.on('-v', '--version', 'Show version') do
          puts "Telegram Silence Enforcer v#{TelegramSilenceEnforcer::VERSION}"
          exit
        end

        opts.on('-h', '--help', 'Show help') do
          puts opts
          exit
        end
      end.parse!

      self.instance_eval(File.read(config_file_name))

      [:telegram_username, :telegram_api_token, :database_path].each do |key|
        unless self.send key
          throw "Config option #{key} is required but not set."
          exit 2
        end
      end

    end

  end

end
