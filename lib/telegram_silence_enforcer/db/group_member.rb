# frozen_string_literal: true

module TelegramSilenceEnforcer

  class DB

    class GroupMember

      attr_reader :id, :telegram_user_id, :username, :first_name, :last_name, :group


      def initialize(db, options = {})
        @db = db
        @id = options[:id] || raise(ArgumentError, 'id is not given')
        @telegram_user_id = options[:telegram_user_id] || raise(ArgumentError, 'telegram_user_id is not given')
        @username = options[:username]
        @first_name = options[:first_name]
        @last_name = options[:last_name]
        @group = options[:group] || raise(ArgumentError, 'group is not given')
        @flag_voice = options[:flag_voice] || false
        @flag_voice_expires = options[:flag_voice_expires]
        @flag_quiet = options[:flag_quiet] || false
        @flag_quiet_expires = options[:flag_quiet_expires]
      end


      def voice(expires = nil)
        expires_at = expires && Time.now.to_i + expires
        @db.orm.from(:group_members).where(id: @id).update(flag_voice: true, flag_voice_expires: expires_at)
        @flag_voice = true
        @flag_voice_expires = expires_at
      end


      def devoice
        @db.orm.from(:group_members).where(id: @id).update(flag_voice: false, flag_voice_expires: nil)
        @flag_voice = false
        @flag_voice_expires = nil
      end


      def voice?
        if @flag_voice_expires && Time.now.to_i > @flag_voice_expires
          devoice
        end
        @flag_voice
      end


      def voice_expires
        @flag_voice_expires && Time.at(@flag_voice_expires)
      end


      def quiet(expires = nil)
        expires_at = expires && Time.now.to_i + expires
        @db.orm.from(:group_members).where(id: @id).update(flag_quiet: true, flag_quiet_expires: expires_at)
        @flag_quiet = true
        @flag_quiet_expires = expires_at
      end


      def unquiet
        @db.orm.from(:group_members).where(id: @id).update(flag_quiet: false, flag_quiet_expires: nil)
        @flag_quiet = false
        @flag_quiet_expires = nil
      end


      def quiet?
        if @flag_quiet_expires && Time.now.to_i > @flag_quiet_expires
          unquiet
        end
        @flag_quiet
      end


      def quiet_expires
        @flag_quiet_expires && Time.at(@flag_quiet_expires)
      end


      def to_s
        @username || @first_name + @last_name.to_s
      end

    end

  end

end
