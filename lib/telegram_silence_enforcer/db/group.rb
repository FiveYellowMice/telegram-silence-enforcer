# frozen_string_literal: true

module TelegramSilenceEnforcer

  class DB

    class Group

      attr_reader :id, :telegram_chat_id


      def initialize(db, options = {})
        @db = db
        @id = options[:id] || raise(ArgumentError, 'id is not given')
        @telegram_chat_id = options[:telegram_chat_id] || raise(ArgumentError, 'telegram_chat_id is not given')
        @mode_moderated = options[:mode_moderated] || false
        @mode_moderated_expires = options[:mode_moderated_expires]
      end


      def moderate(expires = nil)
        expires_at = expires && Time.now.to_i + expires
        @db.orm.from(:groups).where(id: @id).update(mode_moderated: true, mode_moderated_expires: expires_at)
        @mode_moderated = true
        @mode_moderated_expires = expires_at
      end


      def unmoderate
        @db.orm.from(:groups).where(id: @id).update(mode_moderated: false, mode_moderated_expires: nil)
        @mode_moderated = false
        @mode_moderated_expires = nil
      end


      def moderated?
        if @mode_moderated_expires && Time.now.to_i > @mode_moderated_expires
          unmoderate
        end
        @mode_moderated
      end


      def moderated_expires
        @mode_moderated_expires && Time.at(@mode_moderated_expires)
      end


      def get_or_update_member(obj)
        @db.users_write_lock.lock

        member = nil

        begin
          user_data = @db.orm.from(:users)
            .where(telegram_user_id: obj.id)
            .select(:id, :username, :first_name, :last_name)
            .first

          if user_data
            updating_cols = [:username, :first_name, :last_name].reduce({}) do |m, k|
              m[k] == obj[k] ? m.merge({k => obj[k]}) : m
            end
            if updating_cols.any?
              @db.orm.from(:users).where(id: user_data[:id]).update(updating_cols)
            end
          else
            new_user_id = @db.orm.from(:users).insert(
              telegram_user_id: obj.id,
              username: obj.username,
              first_name: obj.first_name,
              last_name: obj.last_name
            )
            user_data = {
              id: new_user_id
            }
          end

          existing = @db.orm.from(:group_members)
            .where(group: @id, user: user_data[:id])
            .select(:id, :flag_voice, :flag_voice_expires, :flag_quiet, :flag_quiet_expires)
            .first

          if existing
            member = GroupMember.new(@db,
              id: existing[:id],
              telegram_user_id: obj.id,
              username: obj.username,
              first_name: obj.first_name,
              last_name: obj.last_name,
              group: self,
              flag_voice: existing[:flag_voice],
              flag_voice_expires: existing[:flag_voice_expires],
              flag_quiet: existing[:flag_quiet],
              flag_quiet_expires: existing[:flag_quiet_expires]
            )
          else
            new_member_id = @db.orm.from(:group_members).insert(
              user: user_data[:id],
              group: @id
            )
            member = GroupMember.new(@db,
              id: new_member_id,
              telegram_user_id: obj.id,
              username: obj.username,
              first_name: obj.first_name,
              last_name: obj.last_name,
              group: self
            )
          end

        ensure
          @db.users_write_lock.unlock
        end

        member

      end


      def get_member(options = {})
        criterias = options.select do |c, v|
          [:username].include? c
        end

        member_data = @db.orm.from(:group_members)
          .join(:users, id: :user)
          .where({ group: @id }.merge(criterias))
          .select(
            Sequel.qualify(:group_members, :id), :telegram_user_id, :username, :first_name, :last_name,
            :group, :flag_voice, :flag_voice_expires, :flag_quiet, :flag_quiet_expires)
          .first

        if member_data
          GroupMember.new(@db, member_data)
        else
          nil
        end
      end


      def to_s
        @telegram_chat_id.to_s
      end

    end

  end

end
