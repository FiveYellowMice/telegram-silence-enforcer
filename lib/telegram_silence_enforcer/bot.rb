# frozen_string_literal: true

require 'logger'
require 'concurrent'
require 'telegram/bot'

module TelegramSilenceEnforcer

  class Bot

    attr_reader :config, :logger


    def initialize(config)

      @config = config

      STDOUT.sync = true
      @logger = Logger.new(STDOUT, level: @config.debug ? Logger::DEBUG : Logger::INFO)

      @logger.info('Bot') { "Starting Telegram Silence Enforcer v#{TelegramSilenceEnforcer::VERSION}." }

      @db = DB.new(path: @config.database_path, logger: @logger)

      Telegram::Bot::Client.run(@config.telegram_api_token) do |client|
        @api = client.api

        begin
          client.listen do |message|
            Concurrent::Future.execute do
              begin
                receive(message)
              rescue => e
                @logger.error('Bot') { Utils.print_error(e) }
              end
            end
          end
        rescue Telegram::Bot::Exceptions::ResponseError => e
          @logger.error('TelegramApi') { Utils.print_error(e) }
          retry
        rescue Faraday::ConnectionFailed => e
          @logger.error('TelegramApi') { Utils.print_error(e) }
          sleep 1
          retry
        end
      end

    end


    def receive(message)

      @logger.debug('Bot') {
        "Received message: [#{message.from.username || message.from.first_name + message.from.last_name.to_s}] " +
        "#{(message.text || message.caption || '(no text)').gsub("\n", " ")}"
      }

      delete_message = false

      group = @db.get_or_create_group(message.chat)
      member = group.get_or_update_member(message.from)

      if member.quiet?
        delete_message = true
      elsif group.moderated?
        unless member.voice?
          delete_message = true
        end
      end

      if delete_message
        @api.delete_message(
          chat_id: message.chat.id,
          message_id: message.message_id
        )

      elsif message.text && message.text =~ %r[^(?:/\w+@#{Regexp.escape(@config.telegram_username)}|/\w+(?!@))(?: |$)]
        command = Command.new(
          name: /^\/(\w+)/.match(message.text)[1],
          args: /^\/\w+(?:@\w+)? ?(.*)$/.match(message.text)[1],
          original_message: message
        )
        begin
          process_command(command)
        rescue BotOperationError => e
          reply_to(message, text:
            (e.message || 'Something happened.') +
            "\nSend /help@#{@config.telegram_username} for more help."
          )
        rescue => e
          reply_to(message, disable_web_page_preview: true, text:
            "An unexpected error occured during the operation.\n" +
            "#{e.class}: #{e.message}" +
            "\nPlease try again. If the problem persists, please file an issue on https://fym.one/silebot-issues ."
          )
          @logger.error('Bot') { Utils.print_error(e) }
        end
      end

    end


    def process_command(command)

      case command.name
      when 'start'
        if ['group', 'supergroup'].include? command.original_message.chat.type
          reply_to(command.original_message, text: 'Alright. Do what ever you like.')
        else
          raise BotOperationError, 'This bot is only useful in group chats. Please add it to a group.'
        end

      when 'help'
        reply_to(command.original_message,
          disable_web_page_preview: true,
          parse_mode: 'HTML',
          text: HELP_MESSAGE
        )

      when 'status'
        unless ['group', 'supergroup'].include? command.original_message.chat.type
          raise BotOperationError, 'This command only makes sense in group chats. Please add it to a group.'
        end

        group = @db.get_or_create_group(command.original_message.chat)
        member = extract_group_member(group, command)

        if member
          text = member.to_s
          if member.voice?
            text += ' has voice'
            if member.voice_expires
              text += ' till ' + member.voice_expires.utc.to_s
            end
            if member.quiet?
              text += ' and'
            end
          end
          if member.quiet?
            text += ' is quiet'
            if member.quiet_expires
              text += ' till ' + member.quiet_expires.utc.to_s
            end
          end
          unless member.voice? || member.quiet?
            text += ' does not have any flags'
          end
          text += '.'
        else
          text = 'This group is'
          if group.moderated?
            text += ' moderated'
            if group.moderated_expires
              text += ' till ' + group.moderated_expires.utc.to_s
            end
          else
            text += ' not moderated'
          end
          text += '.'
        end

        reply_to(command.original_message, text: text)

      when 'mode'
        unless ['group', 'supergroup'].include? command.original_message.chat.type
          raise BotOperationError, 'This command only makes sense in group chats. Please add it to a group.'
        end

        group = @db.get_or_create_group(command.original_message.chat)

        admin_ids = @api.get_chat_administrators(chat_id: command.original_message.chat.id)['result'].map{|c| c['user']['id'] }
        unless admin_ids.include? command.sender.id
          raise BotOperationError, 'Only administrators are allowed to perform this operation.'
        else
          group.get_or_update_member(command.sender).voice
        end

        case command.args[0]
        when '+m'
          expires = extract_expire_time(command)
          group.moderate(expires)
          @logger.info('Bot') { "Group #{group} is moderated." }
          reply_to(command.original_message,
            text: "This group has been moderated#{expires ? ' till ' + (Time.now.utc + expires).to_s : ''}. " +
              "Only voice users are allowed to talk."
          )
        when '-m'
          group.unmoderate
          @logger.info('Bot') { "Group #{group} is unmoderated." }
          reply_to(command.original_message, text: "This group has been unmoderated. All users are allowed to talk.")
        when '+v'
          member = extract_group_member(group, command) || raise(BotOperationError, "A user must be specified for +v.")
          expires = extract_expire_time(command)
          member.voice(expires)
          @logger.info('Bot') { "User #{member} in group #{group} is voiced." }
          reply_to(command.original_message,
            text: "#{member} has been set voice#{expires ? ' till ' + (Time.now.utc + expires).to_s : ''}."
          )
        when '-v'
          member = extract_group_member(group, command) || raise(BotOperationError, "A user must be specified for -v.")
          if admin_ids.include? member.telegram_user_id
            raise BotOperationError, "You can not devoice an administrator."
          end
          member.devoice
          @logger.info('Bot') { "User #{member} in group #{group} is devoiced." }
          reply_to(command.original_message, text: "#{member} has been unset voice.")
        when '+q'
          member = extract_group_member(group, command) || raise(BotOperationError, "A user must be specified for +q.")
          if admin_ids.include? member.telegram_user_id
            raise BotOperationError, "You can not quiet an administrator."
          end
          expires = extract_expire_time(command)
          member.quiet(expires)
          @logger.info('Bot') { "User #{member} in group #{group} is quieted." }
          reply_to(command.original_message,
            text: "#{member} has been set quiet#{expires ? ' till ' + (Time.now.utc + expires).to_s : ''}."
          )
        when '-q'
          member = extract_group_member(group, command) || raise(BotOperationError, "A user must be specified for -q.")
          member.unquiet
          @logger.info('Bot') { "User #{member} in group #{group} is unquieted." }
          reply_to(command.original_message, text: "#{member} has been unset quiet.")
        else
          raise BotOperationError, "Invalid operation. You can only use: +m, -m, +v, -v, +q, -q."
        end

      end

    end


    def extract_group_member(group, command)
      message = command.original_message

      if entities = message.entities
        entities.reverse.each do |entity|
          case entity.type
          when 'mention'
            username = message.text[entity.offset + 1, entity.length - 1]
            member = group.get_member(username: username)
            if !member
              raise BotOperationError, "User @#{username} is not found."
            end
            return member
          when 'text_mention'
            member = group.get_or_update_member(entity.user)
            return member
          end
        end
      end

      if message.reply_to_message
        member = group.get_or_update_member(message.reply_to_message.from)
        return member
      end

      nil
    end


    def extract_expire_time(command)
      param = command.args[1..-1].find{|s| s.start_with? 'expire=' }
      return nil if param.nil?
      time = Utils.parse_time(param.match(/^expires?\=(.*)$/)[1])

      if time.nil?
        raise BotOperationError, "Invalid time expression."
      elsif time <= 0
        raise BotOperationError, "Expire time must be greater than 0."
      end

      time
    end


    def reply_to(message, options)
      new_options = options.clone
      new_options[:chat_id] = message.chat.id
      new_options[:reply_to_message_id] = message.message_id
      @api.send_message(new_options)
    end


    HELP_MESSAGE = <<~HTML.freeze
      Telegram Silence Enforcer v#{TelegramSilenceEnforcer::VERSION}
      Bring 'moderated', 'voice' and 'quiet' to Telegram.
      <a href="https://gitlab.com/FiveYellowMice/telegram-silence-enforcer">Project Repository</a>
      This program is free software, see <a href="http://www.gnu.org/licenses/">the terms of the GNU General Public License</a> for more information.

      Usage:

      <code>/mode +m [expire=1y2M3d4h5m6s]</code>
      Enable moderated mode for this group. In moderated mode, any user without voice flag can not talk.

      <code>/mode -m</code>
      Disable moderated mode for this group.

      <code>/mode +v @username [expire=1y2M3d4h5m6s]</code>
      Add voice flag to a user. When the group is moderated, such user can talk.

      <code>/mode -v @username</code>
      Remove voice flag from a user.

      <code>/mode +q @username [expire=1y2M3d4h5m6s]</code>
      Add quiet flag to a user. A user can not talk with this flag.

      <code>/mode -q @username</code>
      Remove quiet flag from a user.

      <code>/status [@username]</code>
      Show status of the group or a user.

      <code>/help</code>
      Show help.

      You can reply to a user's message instead of using that user's username.
    HTML

  end

end
