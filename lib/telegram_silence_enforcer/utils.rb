# frozen_string_literal: true

module TelegramSilenceEnforcer

  module Utils

    PARSE_TIME_REGEX = Regexp.new '^' + [
      ['year'  , 'y', 'yr' , 'yrs' , 'years'  ],
      ['month' , 'M', 'mon', 'mons', 'months' ],
      ['day'   , 'd',                'days'   ],
      ['hour'  , 'h', 'hr' , 'hrs' , 'hours'  ],
      ['minute', 'm', 'min', 'mins', 'minutes'],
      ['second', 's', 'sec', 'secs', 'seconds'],
    ].map{|s| "(?:(?<#{s[0]}>\\d+)(?:#{s.join('|')}))?" }.join + '$'


    if ENV['TELEGRAM_SILENCE_ENFORCER_BACKTRACE']
      def self.print_error(e)
        (["#{e.class}: #{e.message}"] + e.backtrace).join("\n    ")
      end
    else
      def self.print_error(e)
        "#{e.class}: #{e.message}"
      end
    end


    def self.parse_time(str)
      matches = str.match(PARSE_TIME_REGEX)
      return nil if matches.nil?

      seconds = matches[:second].to_i
      {year: 31536000, month: 2592000, day: 86400, hour: 3600, minute: 60}.each do |unit, worth|
        seconds += matches[unit].to_i * worth
      end
      seconds
    end

  end

end
