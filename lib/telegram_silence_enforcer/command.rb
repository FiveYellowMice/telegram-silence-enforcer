# frozen_string_literal: true

require 'shellwords'

module TelegramSilenceEnforcer

  class Command

    attr_reader :name, :args, :original_message

    def initialize(options = {})
      @name = options[:name]
      @args = options[:args]
      @original_message = options[:original_message]

      unless @args.is_a? Array
        @args = @args.to_s.shellsplit
      end
    end


    def sender
      original_message.from
    end


    def text
      original_message.text
    end

  end

end
