# frozen_string_literal: true

require 'sequel'

module TelegramSilenceEnforcer

  class DB

    attr_reader :orm, :logger, :groups_write_lock, :users_write_lock


    def initialize(options = {})
      @path = options[:path] || raise(ArgumentError, 'No path has been given to the database')
      @logger = options[:logger] || raise(ArgumentError, 'No logger has been given to the database')

      need_create_table = !File.exist?(@path)
      @orm = Sequel.sqlite(@path)
      if need_create_table
        create_tables
        @logger.info('DB') { 'Tables have been created.' }
      end

      @groups_write_lock = Thread::Mutex.new
      @users_write_lock = Thread::Mutex.new

      @logger.debug('DB') { 'Database is ready.' }
    end


    def create_tables
      @orm.run <<-SQL
        PRAGMA foreign_keys = ON;

        CREATE TABLE db_version (
          version                INTEGER NOT NULL
        );
        INSERT INTO db_version (version) VALUES (1);

        CREATE TABLE groups (
          id                     INTEGER PRIMARY KEY,
          telegram_chat_id       INTEGER UNIQUE NOT NULL,
          mode_moderated         BOOLEAN NOT NULL DEFAULT FALSE,
          mode_moderated_expires INTEGER
        );

        CREATE TABLE users (
          id                     INTEGER PRIMARY KEY,
          telegram_user_id       INTEGER UNIQUE NOT NULL,
          username               TEXT,
          first_name             TEXT,
          last_name              TEXT
        );

        CREATE TABLE group_members (
          id                     INTEGER PRIMARY KEY,
          user                   INTEGER NOT NULL REFERENCES users(id),
          `group`                INTEGER NOT NULL REFERENCES groups(id),
          flag_voice             BOOLEAN NOT NULL DEFAULT FALSE,
          flag_voice_expires     INTEGER,
          flag_quiet             BOOLEAN NOT NULL DEFAULT FALSE,
          flag_quiet_expires     INTEGER
        );
      SQL
    end

    private :create_tables


    def get_or_create_group(obj)
      @groups_write_lock.lock

      group = nil

      begin
        existing = @orm.from(:groups)
          .where(telegram_chat_id: obj.id)
          .select(:id, :mode_moderated, :mode_moderated_expires)
          .first

        if existing
          group = Group.new(self,
            id: existing[:id],
            telegram_chat_id: obj.id,
            mode_moderated: existing[:mode_moderated],
            mode_moderated_expires: existing[:mode_moderated_expires]
          )
        else
          new_id = @orm.from(:groups).insert(telegram_chat_id: obj.id)
          group = Group.new(self,
            id: new_id,
            telegram_chat_id: obj.id
          )
        end

      ensure
        @groups_write_lock.unlock
      end

      group

    end


    autoload :Group, 'telegram_silence_enforcer/db/group'
    autoload :GroupMember, 'telegram_silence_enforcer/db/group_member'

  end

end
