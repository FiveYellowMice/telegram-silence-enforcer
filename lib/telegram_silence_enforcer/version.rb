# frozen_string_literal: true

module TelegramSilenceEnforcer

  VERSION = Object.new

  def VERSION.major
    0
  end

  def VERSION.minor
    2
  end

  def VERSION.patch
    0
  end

  VERSION.define_singleton_method :[] do |s|
    case s
    when 0
      VERSION.major
    when 1
      VERSION.minor
    when 2
      VERSION.patch
    else
      nil
    end
  end

  def VERSION.to_s
    "#{self.major}.#{self.minor}.#{self.patch}"
  end

  def VERSION.inspect
    self.to_s
  end

end
